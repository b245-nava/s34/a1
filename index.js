const express = require('express');

const port = 4000;

const app = express();

app.use(express.json());

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }

];

// Get /home
app.get("/home", (req, res) => {
	res.send("Welcome to the Armory!");
});

// Get /items
app.get("/items", (req, res) => {
	res.send(items);
});

// Delete /delete-item
app.delete("/delete-item", (req, res) => {

	let deletedItem = items.pop();

	res.send(`The item ${deletedItem.name} has been discarded.`);
})

app.listen(port, () => console.log(`Server now active at port ${port}.`));